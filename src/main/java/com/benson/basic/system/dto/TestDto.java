package com.benson.basic.system.dto;

import com.benson.common.common.validate.self.VerifyOauth;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 自定义验证
 *
 * @author zhangby
 * @date 20/10/20 5:50 pm
 */
@Data
public class TestDto {

    @VerifyOauth(role = "admin")
    private String role;

    @NotBlank(message = "不能为空")
    private String userId;
}

