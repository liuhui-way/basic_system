package com.benson.basic.system.cron;

import cn.hutool.core.date.DateTime;
import com.benson.common.cron.annotation.CronConfig;
import com.benson.common.cron.cron.BaseCron;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 系统定时任务
 *
 * @author zhangby
 * @date 9/10/20 11:09 am
 */
@CronConfig(cron = "0 * * * * ?", value = "SysCronMinute", description = "系统默认定时任务(分)")
public class SysCronMinute extends BaseCron {

    /**
     * 定时任务实现接口
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(new DateTime());
    }
}
