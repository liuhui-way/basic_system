package com.benson.basic.system.cron;

import cn.hutool.core.date.DateTime;
import com.benson.common.cron.annotation.CronConfig;
import com.benson.common.cron.cron.BaseCron;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * 定时任务
 *
 * @author zhangby
 * @date 15/10/20 3:56 pm
 */
@CronConfig(cron = "* * * * * ?", value = "SysCronSecond", description = "系统默认定时任务(秒)")
public class SysCronSecond extends BaseCron {

    /**
     * 实现定时任务
     */
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(new DateTime());
    }
}
