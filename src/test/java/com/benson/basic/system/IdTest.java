package com.benson.basic.system;

import cn.hutool.core.util.IdUtil;
import org.assertj.core.util.Lists;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class IdTest {
    public static void main(String[] args) {
        List<String> idList = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            String objectId = IdUtil.objectId();
            idList.add(objectId);
            System.out.println(objectId);
        }

        System.out.println(idList);
        System.out.println(idList.stream()
                .sorted(Comparator.comparing(String::toString))
                .collect(Collectors.toList())
        );
    }
}
